### `npm start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.\
You will also see any lint errors in the console.

### `npm test`

Launches the test runner in the interactive watch mode.\
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### `npm run build`

### Crear Proyecto con Typescript
~~~
npx create-react-app my-app --template typescript
~~~

### Iconos SVG

* https://heroicons.dev/

numberFiled: Yup.number()
                        .typeError('you must specify a number')
                        .min(0, 'Min value 0.')
                        .max(30, 'Max value 30.')