import { Link } from 'react-router-dom'
import { Dispatch, SetStateAction } from "react"
import "./LinkSidebar.css"
import Icon from '../../atoms/Icon/Icon'

interface ILinkSidebarProps {
  selectedMenu: string
  setSelectedMenu: Dispatch<SetStateAction<string>>
  url: string
  title: string
  icon: string
}

const LinkSidebar = (props: ILinkSidebarProps) => {

  const { selectedMenu, setSelectedMenu, url, title, icon } = props

  return (
    <Link
      to={url}
      className={`${selectedMenu === url ? 'menu-active' : 'menu-inactive'} flex items-center mt-4 py-2 px-6`}
      onClick={ () => setSelectedMenu(url)}
    >
      <Icon icon={icon} />
      <span className="mx-3">{ title }</span>
    </Link>
  )
}

export default LinkSidebar
