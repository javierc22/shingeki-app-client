import TdCreatedAt from "../../atoms/TdCreatedAt/TdCreatedAt"
import TdName from "../../atoms/TdName/TdName"
import TdOptions from "../../atoms/TdOptions/TdOptions"
import TdStatus from "../../atoms/TdStatus/TdStatus"

type ItemTableProps = {
  key: number
  item: Titan
  type: string
}

type Titan = {
  id?: string | undefined
  name: string
  userName: string
  imageUrl?: string
  createdAt: string
  active: boolean
}

const ItemTable = ({item, type}: ItemTableProps) => {
  return (
    <tr>
      <TdName 
        name={item.name} 
        userName={item.userName} 
        id={item.id} 
        imageUrl={item.imageUrl}
        type={type}
      />
      <TdCreatedAt createdAt={item.createdAt } />
      <TdStatus active={item.active} />
      <TdOptions id={item.id} type={type} />
    </tr>
  )
}

export default ItemTable
