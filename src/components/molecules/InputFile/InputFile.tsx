import React, {useState, Dispatch, SetStateAction} from 'react'

type InputProps = {
  label: string
  required: boolean
  imageUrl?: string | undefined
  setFile: Dispatch<SetStateAction<File | undefined>>
};

const InputFile: React.FC<InputProps> = (props) => {

  const { label, required, setFile, imageUrl } = props

  const [errorFile, setErrorFile] = useState<boolean>(false)
  const [message, setMessage] = useState<string>("")
  const [previewImage, setPreviewImage] = useState<string>()

  const handleSetFile = (event: React.ChangeEvent<HTMLInputElement>) => {
    if (!event.target.files) return

    const fileImage = event.target.files[0]
    const imageObject = new Image()
    const imageUrl = URL.createObjectURL(fileImage)
    imageObject.src = imageUrl

    if (validateFormat(fileImage) && validateSize(fileImage)) {
      setFile(fileImage)
      setPreviewImage(imageUrl)
      setMessage("")
    } else {
      setErrorFile(true)
      setPreviewImage("")
      setFile(undefined)
      return
    }

    imageObject.onload = () => {
      if (imageObject.width > 400 || imageObject.height > 600) {
        setMessage("Tamaño de imagen supera las dimensiones de 400 x 600")
        setErrorFile(true)
        setPreviewImage("")
        setFile(undefined)
        return
      }
    }
  }

  const validateFormat = (file: File) => {
    if (file.type === "image/jpeg" || file.type === "image/png" || file.type === "image/jpg") {
      return true
    } else {
      setMessage("Formato no permitido")
      return false
    }
  }

  const validateSize = (file: File) => {
    if (file.size < 200000) {
      return true
    } else {
      setMessage("Tamaño de imagen supera los 200KB")
      return false
    }
  }

  return (
    <>
      { (previewImage || imageUrl) && <img src={previewImage ? previewImage : imageUrl} alt="preview_image" style={{ height: "300px", objectFit: "cover" }}></img> }

      <label className="block text-sm font-medium text-gray-700">
        {label}
        <span className="text-red-500">
          {required && '*'}
        </span>
      </label>
        <p className="mt-1 text-sm text-gray-600">
          Formatos permitidos: .jpeg y .png
        </p>
        <p className="mt-1 text-sm text-gray-600">
          Tamaño máximo: 200 KB
        </p>
      <input 
        type="file"
        onChange={handleSetFile}
      />

      <p className="text-sm text-red-500 -bottom-6">
        {errorFile && message}
      </p>
    </>
  )
}

export default InputFile
