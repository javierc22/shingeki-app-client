import React from 'react'
import { Path, UseFormRegister} from "react-hook-form"
import "./InputCharacterForm.css"

interface IFormValues {
  name: string
  lastname: string
  extract: string
  description: string
  gender: string
  active: boolean
}

type InputProps = {
  label: string
  type: string
  name: Path<IFormValues>
  register: UseFormRegister<IFormValues>
  required: boolean
  errors?: any
  errorMessage?: string
};

const InputCharacterForm: React.FC<InputProps> = (props) => {

  const { label, type, name, register, required, errors } = props

  return (
    <>
      <label className="block text-sm font-medium text-gray-700">
        {label}
        <span className="text-red-500">
          {required && '*'}
        </span>
      </label>
      <input 
        type={type}
        className={errors ? 'input-form-error' : 'input-form'}
        {...register(name, { required })}
      />

      <p className="text-sm text-red-500 -bottom-6">
        {errors?.message}
      </p>
    </>
  )
}

export default InputCharacterForm
