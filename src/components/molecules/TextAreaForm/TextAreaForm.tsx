import React from 'react';
import { Path, UseFormRegister} from "react-hook-form"
import "./TextAreaForm.css"

interface IFormValues {
  name: string
  userName: string
  height: number
  extract: string
  description: string
  active: boolean
}

type InputProps = {
  label: string
  name: Path<IFormValues>
  register: UseFormRegister<IFormValues>;
  required: boolean;
};

const TextAreaForm: React.FC<InputProps> = (props) => {

  const {label, name, register, required } = props

  return (
    <>
      <label className="block text-sm font-medium text-gray-700">
        {label}
      </label>
      <textarea
        className="textarea-form"
        {...register(name, { required })}
      ></textarea>
    </>
  )
}

export default TextAreaForm
