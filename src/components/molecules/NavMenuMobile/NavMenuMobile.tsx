import ButtonMenuClose from '../../atoms/ButtonMenuClose/ButtonMenuClose'
import LogoMobile from '../../atoms/LogoMobile/LogoMobile'
import NavLinkMobile from '../../atoms/NavLinkMobile/NavLinkMobile'

interface NavMenuMobileProps {
  handleCloseMenu: (event: React.MouseEvent<HTMLButtonElement>) => void
}

const NavMenuMobile = ({handleCloseMenu}: NavMenuMobileProps) => {
  return (
    <div className="absolute top-0 inset-x-0 p-2 transition transform origin-top-right md:hidden">
      <div className="rounded-lg shadow-lg ring-1 ring-black ring-opacity-5 bg-white divide-y-2 divide-gray-50">
        <div className="pt-5 pb-6 px-5">
          <div className="flex items-center justify-between">
            <div>
              <LogoMobile />
            </div>
            <div className="-mr-2">
              <ButtonMenuClose handleCloseMenu={handleCloseMenu} />
            </div>
          </div>

          <div className="mt-6">
            <nav className="grid gap-y-8">
              <NavLinkMobile title="Inicio" iconName="home" />
              <NavLinkMobile title="Personajes" iconName="cursor-click" />
              <NavLinkMobile title="Titanes" iconName="shield-check" />
              <NavLinkMobile title="Curiosidades" iconName="view-grid" />
            </nav>
          </div>
        </div>
      </div>
    </div>
  )
}

export default NavMenuMobile
