import React from 'react';
import { Path, UseFormRegister} from "react-hook-form"
import "./TextAreaCharacterForm.css"

interface IFormValues {
  name: string
  lastname: string
  extract: string
  description: string
  gender: string
  active: boolean
}

type InputProps = {
  label: string
  name: Path<IFormValues>
  register: UseFormRegister<IFormValues>;
  required: boolean;
};

const TextAreaCharacterForm: React.FC<InputProps> = (props) => {

  const {label, name, register, required } = props

  return (
    <>
      <label className="block text-sm font-medium text-gray-700">
        {label}
      </label>
      <textarea
        className="textarea-form"
        {...register(name, { required })}
      ></textarea>
    </>
  )
}

export default TextAreaCharacterForm
