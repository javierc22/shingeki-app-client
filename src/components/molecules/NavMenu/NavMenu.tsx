import React from 'react'
import NavLink from '../../atoms/NavLink/NavLink'

const NavMenu = () => {
  return (
    <>
      <NavLink title={"Inicio"} link={"/"} />
      <NavLink title={"Personajes"} link={"personajes"} />
      <NavLink title={"Titanes"} link={"/titanes"} />
      <NavLink title={"Curiosidades"} link={"/curiosidades"} />
    </>
  )
}

export default NavMenu
