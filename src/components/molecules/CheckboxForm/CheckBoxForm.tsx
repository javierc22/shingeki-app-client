import React from 'react'
import { Path, UseFormRegister} from "react-hook-form"

interface IFormValues {
  name: string
  userName: string
  height: number
  extract: string
  description: string
  active: boolean
}

type CheckboxProps = {
  name: Path<IFormValues>
  register: UseFormRegister<IFormValues>
  required: boolean
}

const CheckBoxForm: React.FC<CheckboxProps> = (props) => {

  const {name, register, required } = props

  return (
    <>
      <div className="flex items-center py-1">
        <input 
          type="checkbox" 
          className="focus:ring-indigo-500 h-5 w-5 text-indigo-600 border-gray-300 rounded"
          {...register(name, { required })}
        />
      </div>
      <div className="ml-3 text-sm py-1">
        <label className="font-medium text-gray-700">Activo</label>
      </div> 
    </>
  )
}

export default CheckBoxForm
