import "./TdStatus.css"

interface TdStatusProps {
  active: boolean
}

const TdStatus = ({ active }: TdStatusProps) => {
  return (
    <td className="px-6 py-4 whitespace-no-wrap border-b border-gray-200">
      <span className={`${active ? 'span-active' : 'span-inactive'}`}>
        { active ? "Activo" : "Inactivo" }
      </span>
    </td>
  )
}

export default TdStatus
