type ImageShowProps = {
  imageSrc: string
}

const ImageShow = ({ imageSrc }: ImageShowProps) => {
  return (
    <img 
      src={imageSrc} 
      className="rounded-lg w-full md:w-auto md:max-h-80 sm:object-contain" alt="titan"
    ></img>
  )
}

export default ImageShow
