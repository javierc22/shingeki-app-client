import RefreshIcon from "../Icons/RefreshIcon";
import "./ButtonPrimary.css";

type ButtonPrimaryProps = {
  text: string;
  type: "button" | "submit" | "reset";
  loading: boolean;
};

const ButtonPrimary = ({ text, type, loading }: ButtonPrimaryProps) => {
  return (
    <button
      type={type}
      className={`${loading ? "disabled:opacity-50" : ""} btn-primary`}
      disabled={loading}
    >
      {loading && <RefreshIcon />}

      {text}
    </button>
  );
};

export default ButtonPrimary;
