import { Link } from "react-router-dom"
import logo from '../../../assets/img/logo.png'

const Logo = () => {
  return (
    <Link to="/">
      <img
        className="h-8 w-auto sm:h-10"
        src={logo}
        alt="logo"
      ></img>
    </Link>
  )
}

export default Logo
