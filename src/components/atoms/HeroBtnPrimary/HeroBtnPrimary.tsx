
type HeroBtnPrimaryProps = {
  title: string
}

const HeroBtnPrimary = ({title}: HeroBtnPrimaryProps) => {
  return (
    <button className="apple shadow-md px-3 py-2 rounded-lg flex items-center text-white bg-yellow-600 hover:bg-yellow-700">
      {title}
    </button>
  )
}

export default HeroBtnPrimary
