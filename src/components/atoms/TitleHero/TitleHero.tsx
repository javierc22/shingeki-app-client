import React from 'react'

type HeroProps = {
  title: string
}

const TitleHero = ({title}:HeroProps) => {
  return (
    <h1 className="font-bold text-4xl md:text-5xl max-w-xl text-gray-900 leading-tight text-center md:text-left">
      {title}
    </h1>
  )
}

export default TitleHero
