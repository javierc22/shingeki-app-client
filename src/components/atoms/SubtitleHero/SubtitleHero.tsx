import React from 'react'

type SubtitleHeroProps = {
  subtitle: string
}

const SubtitleHero = ({subtitle}: SubtitleHeroProps) => {
  return (
    <p className="text-gray-800 text-base leading-relaxed mt-8 font-semibold text-center md:text-left">
      {subtitle}
    </p>
  )
}

export default SubtitleHero
