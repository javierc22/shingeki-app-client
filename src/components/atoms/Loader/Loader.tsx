import Lottie from "lottie-react"
import loadingAnimation from "./6722-loading.json"

const Loader = () => {
  return (
    <div style={{ display: "flex", flexDirection: "column", alignItems: "center" }}>
      <Lottie animationData={loadingAnimation} height={400} width={400} />
    </div>
  )
}

export default Loader
