import React from 'react'
import HomeSvg from '../Icons/HomeSvg'
import CursorClickSvg from '../Icons/CursorClickSvg'
import ShieldCheckSvg from '../Icons/ShieldCheckSvg'
import ViewGridSvg from '../Icons/ViewGridSvg'
import AddSvg from '../Icons/AddSvg'
import SidebarIcon1 from '../Icons/SidebarIcon1'
import SidebarIcon2 from '../Icons/SidebarIcon2'
import SidebarIcon3 from '../Icons/SidebarIcon3'
import SidebarIcon4 from '../Icons/SidebarIcon4'

type IconProps = {
  icon: string
}

const Icon = ({icon}:IconProps) => {

  switch (icon) {
    case 'home':
      return <HomeSvg />

    case 'cursor-click':
      return <CursorClickSvg />

    case 'shield-check':
      return <ShieldCheckSvg />

    case 'view-grid':
      return <ViewGridSvg />

    case 'add':
      return <AddSvg />

    case 'sidebar-icon-1':
      return <SidebarIcon1 />

    case 'sidebar-icon-2':
      return <SidebarIcon2 />

    case 'sidebar-icon-3':
      return <SidebarIcon3 />

    case 'sidebar-icon-4':
      return <SidebarIcon4 />
  
    default:
      return <HomeSvg />
  }
}

export default Icon
