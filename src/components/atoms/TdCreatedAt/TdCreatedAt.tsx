import useDateFormat from "../../../hooks/useDateFormat"

type TdCreatedAtProps = {
  createdAt: string
}

const TdCreatedAt = ({createdAt}: TdCreatedAtProps) => {
  const { dateFormatted } = useDateFormat(createdAt)
  
  return (
    <td className="px-6 py-4 whitespace-no-wrap border-b border-gray-200">
      <div className="text-sm leading-5 text-gray-900">
        { dateFormatted }
      </div>
    </td>
  )
}

export default TdCreatedAt
