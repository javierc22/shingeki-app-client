import logo from '../../../assets/img/logo.png'

const LogoMobile = () => {
  return (
    <img
      className="h-8 w-auto"
      src={logo}
      alt="Workflow"
    ></img>
  )
}

export default LogoMobile
