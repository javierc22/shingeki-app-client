import "./ButtonDanger.css"

type ButtonDangerProps = {
  text: string
  type: "button" | "submit" | "reset" | undefined
  handleOnClick: (event: React.MouseEvent<HTMLButtonElement>) => void
}

const ButtonDanger = ({type, text, handleOnClick}: ButtonDangerProps) => {
  return (
    <button type={type} className="btn-danger" onClick={handleOnClick}>
      { text }
    </button>
  )
}

export default ButtonDanger
