import { Link } from 'react-router-dom'
import Icon from '../Icon/Icon'
import "./ButtonAddNew.css"

type ButtonAddNewProps = {
  url: string
}

const ButtonAddNew = ({ url }: ButtonAddNewProps) => {
  return (
    <Link to={url}>   
      <button className="btn-add-new">
        <Icon icon={"add"} />
        <span>Nuevo</span>
      </button>
    </Link>
  )
}

export default ButtonAddNew
