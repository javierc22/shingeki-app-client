import React from 'react'
import { Link } from "react-router-dom"
import "./navlink.css"

type NavLinkProps = {
  title: string,
  link: string
}

const NavLink = ({title, link}: NavLinkProps) => {
  return (
    <Link
      to={link}
      className="nav-link"
    >
      {title}
    </Link>
  )
}

export default NavLink
