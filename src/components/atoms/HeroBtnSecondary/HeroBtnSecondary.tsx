import React from 'react'

type HeroBtnSecondaryProps = {
  title: string
}

const HeroBtnSecondary = ({title}: HeroBtnSecondaryProps) => {
  return (
    <button className="google bg-white shadow-md px-3 py-2 rounded-lg flex items-center">
      {title}
    </button>
  )
}

export default HeroBtnSecondary
