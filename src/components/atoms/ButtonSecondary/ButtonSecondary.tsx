import "./ButtonSecondary.css"

type ButtonSecondaryProps = {
  text: string
  type: "button" | "submit" | "reset" | undefined
}

const ButtonSecondary = ({text, type}: ButtonSecondaryProps) => {
  return (
    <button type={type} className="btn-secondary">
      { text }
    </button>
  )
}

export default ButtonSecondary
