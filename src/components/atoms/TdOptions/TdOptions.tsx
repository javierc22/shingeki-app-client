import { Link } from 'react-router-dom'

type TdOptionsProps = {
  id: string | undefined
  type: string
}
const TdOptions = ({id, type}: TdOptionsProps) => {

  return (
    <td className="px-6 py-4 whitespace-no-wrap text-right border-b border-gray-200 text-sm leading-5 font-medium">
      <Link to={`./${type}/${id}/editar`}>
        <p
          className="text-indigo-600 hover:text-indigo-900"
        >
          Editar
        </p>
      </Link>
    </td>
  )
}

export default TdOptions
