import React from 'react'
import { Link } from 'react-router-dom'
import Icon from '../Icon/Icon'

type NavLinkMobileProps = {
  title: string,
  iconName?: string
}

const NavLinkMobile = ({title, iconName}: NavLinkMobileProps) => {
  
  return (
    <Link
      to="/"
      className="-m-3 p-3 flex items-center rounded-md hover:bg-gray-50"
    >
      <Icon icon={iconName!} />
      <span className="ml-3 text-base font-medium text-gray-900">
        {title}
      </span>
    </Link>
  )
}

export default NavLinkMobile
