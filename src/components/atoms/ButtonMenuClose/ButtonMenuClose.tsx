import CloseSvg from '../Icons/CloseSvg'
import "./buttonmenuclose.css"

interface ButtonMenuCloseProps {
  handleCloseMenu: (event: React.MouseEvent<HTMLButtonElement>) => void
}

const ButtonMenuClose = ({handleCloseMenu}:ButtonMenuCloseProps) => {
  return (
    <button
      type="button"
      className="btn-menu-close"
      onClick={handleCloseMenu}
    >
      <span className="sr-only">Close menu</span>
      <CloseSvg />
    </button>
  )
}

export default ButtonMenuClose
