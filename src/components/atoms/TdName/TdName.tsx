import { Link } from 'react-router-dom'
import defaultImage from '../../../assets/img/default_image.jpg'

type TdNameProps = {
  id: string | undefined
  name: string
  userName?: string
  imageUrl?: string
  type: string
}

const TdName = ({id, name, userName, imageUrl, type}: TdNameProps) => {
  return (
    <td className="px-6 py-4 whitespace-no-wrap border-b border-gray-200">
      <Link to={`./${type}/${id}`}>
        <div className="flex items-center">
          <div className="flex-shrink-0 h-10 w-10">
            <img
              className="h-10 w-10 rounded-full"
              src={imageUrl ? imageUrl : defaultImage}
              alt=""
            ></img>
          </div>

          <div className="ml-4">
            <div className="text-sm leading-5 font-medium text-gray-900">
              {name}
            </div>
            <div className="text-sm leading-5 text-gray-500">
              {userName}
            </div>
          </div>
        </div>
      </Link>
    </td>
  )
}

export default TdName
