import "./AlertError.css"

type AlertErrorType = {
  error: boolean
}

const AlertError = ({error}: AlertErrorType) => {
  return (
    <div className={`${error ? "j-transition-in" : "j-transition-out"} alert-error`} role="alert">
      <span>Algo salió mal :(</span>
      <button className="w-4" type="button" data-dismiss="alert" aria-label="Close">
          <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor">
              <path strokeLinecap="round" strokeLinejoin="round" strokeWidth={2} d="M6 18L18 6M6 6l12 12" />
          </svg>
      </button>
    </div>
  )
}

export default AlertError
