interface IContentShowProps {
  extract: string
  description: string
}


const ContentShow = (props: IContentShowProps) => {

  const { extract, description } = props

  return (
    <div className="shadow overflow-hidden sm:rounded-md mt-3 md:min-h-full bg-white">
      <div className="px-5 pt-5">
        <p className="text-gray-700 text-2lg font-medium">
          Extracto:
        </p>
        <p className="text-gray-700 text-2lg">
          { extract }
        </p>
      </div>

      <div className="px-5 py-5">
        <p className="text-gray-700 text-2lg font-medium">
          Descripción:
        </p>
        <p className="text-gray-700 text-2lg">
          { description }
        </p>
      </div>
    </div>
  )
}

export default ContentShow
