import ItemTable from "../../../molecules/ItemTable/ItemTable"
import ThTable from "../../../molecules/ThTable/ThTable"

type DashboardTableProps = {
  items: ItemType[]
  type: string
}

type ItemType = {
  id?: string | undefined
  name: string
  userName: string
  createdAt: string
  active: boolean
}

const DashboardTable = ({ items, type }: DashboardTableProps) => {
  if (items.length === 0) return <p>No hay elementos a mostrar</p>

  return (
    <table className="min-w-full">
      <thead>
        <tr>
          <ThTable title="Nombre"/>
          <ThTable title="Fecha creación"/>
          <ThTable title="Estado"/>
          <ThTable />
        </tr>
      </thead>

      <tbody className="bg-white">
        {
          items.map( (item, index) => (
            <ItemTable key={index} item={item} type={type} />
          )) 
        }
      </tbody>
    </table>
  )
}

export default DashboardTable
