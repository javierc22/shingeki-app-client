import * as yup from "yup"

const schemaValidation = yup.object().shape({
  name: yup.string()
    .required("Nombre es requerido")
    .min(2, "Nombre muy corto"),
  userName: yup.string()
    .required("Nombre es requerido")
    .min(2, "Nombre muy corto"),
  height: yup.number()
    .typeError('Debe especificar un número')
    .positive("Debe ser un número positivo")
})


export default schemaValidation
