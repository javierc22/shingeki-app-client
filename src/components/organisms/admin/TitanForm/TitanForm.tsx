import { useContext, useState } from 'react'
import { useForm } from "react-hook-form"
import { yupResolver } from '@hookform/resolvers/yup'

import ButtonPrimary from "../../../atoms/ButtonPrimary/ButtonPrimary"
import InputForm from "../../../molecules/InputForm/InputForm"
import InputFile from "../../../molecules/InputFile/InputFile"
import TextAreaForm from "../../../molecules/TextAreaForm/TextAreaForm"
import schemaValidation from "./schemaValidation"
import { ITitan } from '../../../../context/admin/titans/TitanState'
import TitanContext from "../../../../context/admin/titans/TitanContext"
import CheckBoxForm from '../../../molecules/CheckboxForm/CheckBoxForm'

type TitanFormProps = {
  titan: ITitan
  submitForm: (payload:ITitan) => void
}

const TitanForm = ({titan, submitForm}: TitanFormProps) => {
  const { register, handleSubmit, formState: { errors } } = useForm<ITitan>({ 
    defaultValues: titan, 
    resolver: yupResolver(schemaValidation) 
  })

  const [file, setFile] = useState<File | undefined>()
  const { loading } = useContext(TitanContext)

  const onSubmit = ((data:ITitan) => {
    submitForm({...data, image: file})
  })

  return (
    <form onSubmit={handleSubmit(onSubmit)}>
      <div className="shadow overflow-hidden sm:rounded-md">
        <div className="px-4 py-5 bg-white sm:p-6">
          <div className="grid grid-cols-6 gap-6">
            <div className="col-span-6 sm:col-span-3">
              <InputForm 
                label={"Nombre"} 
                type={"text"} 
                name={"name"}
                register={register}
                errors={errors.name}
                required
              />
            </div>

            <div className="col-span-6 sm:col-span-3">
              <InputForm 
                label={"Nombre portador"} 
                type={"text"}
                name={"userName"}
                register={register}
                errors={errors.userName}
                required
              />
            </div>

            <div className="col-span-6 sm:col-span-6">
              <InputForm 
                label={"Extracto"} 
                type={"text"}
                name={"extract"}
                register={register}
                required={false}
              />
            </div>

            <div className="col-span-6 sm:col-span-6">
              <TextAreaForm 
                label={"Descripción"}
                name={"description"}
                register={register}
                required={false}
              />
            </div>

            <div className="col-span-6 sm:col-span-1">
              <InputForm 
                label={"Altura (metros)"} 
                type={"number"} 
                name={"height"}
                register={register}
                errors={errors.height}
                required
              />
            </div>

            <div className="col-span-6 sm:col-span-1 flex items-end">
              <CheckBoxForm 
                name={"active"}
                register={register}
                required={false}
              />
            </div>

            <div className="col-span-6 sm:col-span-6">
              <InputFile
                imageUrl={titan.imageUrl}
                label={"Imagen"}
                required={false}
                setFile={setFile}
              />
            </div>
          </div>
        </div>
        <div className="px-4 py-3 bg-gray-50 text-right sm:px-6">
          <ButtonPrimary text="Guardar" type="submit" loading={loading} />
        </div>
      </div>
    </form>
  )
}

export default TitanForm
