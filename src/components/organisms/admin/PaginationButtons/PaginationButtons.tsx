import "./PaginationButtons.css"

type PaginationButtonsType = {
  page: Number,
  paginationInfo: {
    totalPages: Number,
    totalDocs: Number
  }
  nextPage: () => void
  previousPage: () => void
}

const PaginationButtons = ({page, paginationInfo, nextPage, previousPage}: PaginationButtonsType) => {
  const { totalPages, totalDocs } = paginationInfo

  const isStartPage = page === 1
  const isEndPage = page === totalPages

  return (
    <div className="px-5 py-5 bg-white border-t flex flex-col xs:flex-row items-center xs:justify-between">
      <span className="text-xs xs:text-sm text-gray-900">
        {`Showing ${page} to ${totalPages} of ${totalDocs} Entries`}
      </span>
      <div className="inline-flex mt-2 xs:mt-0">
        <button
          className={`${isStartPage ? "btn-pagination-disabled" : "btn-pagination"} rounded-l`}
          disabled={isStartPage}
          onClick={previousPage}
        >
          Anterior
        </button>
        <button 
          className={`${isEndPage ? "btn-pagination-disabled" : "btn-pagination"} rounded-r`}
          disabled={isEndPage}
          onClick={nextPage}
        >
          Siguiente
        </button>
      </div>
    </div>
  )
}

export default PaginationButtons
