import * as yup from "yup"

const schemaValidation = yup.object().shape({
  name: yup.string()
    .required("Nombre es requerido")
    .min(2, "Nombre muy corto"),
  lastname: yup.string()
    .required("Apellido es requerido")
    .min(2, "Apellido muy corto"),
})


export default schemaValidation
