import { useContext, useState } from 'react'
import { useForm } from "react-hook-form"
import { yupResolver } from '@hookform/resolvers/yup'

import ButtonPrimary from "../../../atoms/ButtonPrimary/ButtonPrimary"
import InputFile from "../../../molecules/InputFile/InputFile"
import schemaValidation from "./schemaValidation"
import { ICharacter } from '../../../../context/admin/characters/CharacterState'
import CharacterContext from "../../../../context/admin/characters/CharacterContext"
import InputCharacterForm from '../../../molecules/InputCharacterForm/InputCharacterForm'
import TextAreaCharacterForm from '../../../molecules/TextAreaCharacterForm/TextAreaCharacterForm'
import CheckBoxCharacterForm from '../../../molecules/CheckboxCharacterForm/CheckBoxCharacterForm'

type TitanFormProps = {
  character: ICharacter
  submitForm: (payload:ICharacter) => void
}

const TitanForm = ({character, submitForm}: TitanFormProps) => {
  const { register, handleSubmit, formState: { errors } } = useForm<ICharacter>({ 
    defaultValues: character, 
    resolver: yupResolver(schemaValidation) 
  })

  const [file, setFile] = useState<File | undefined>()
  const { loading } = useContext(CharacterContext)

  const onSubmit = ((data:ICharacter) => {
    console.log(data)
    submitForm({...data, image: file})
  })

  return (
    <form onSubmit={handleSubmit(onSubmit)}>
      <div className="shadow overflow-hidden sm:rounded-md">
        <div className="px-4 py-5 bg-white sm:p-6">
          <div className="grid grid-cols-6 gap-6">
            <div className="col-span-6 sm:col-span-3">
              <InputCharacterForm
                label={"Nombre"} 
                type={"text"} 
                name={"name"}
                register={register}
                errors={errors.name}
                required
              />
            </div>

            <div className="col-span-6 sm:col-span-3">
              <InputCharacterForm 
                label={"Apellido"} 
                type={"text"}
                name={"lastname"}
                register={register}
                errors={errors.lastname}
                required
              />
            </div>

            <div className="col-span-6 sm:col-span-6">
              <InputCharacterForm 
                label={"Extracto"} 
                type={"text"}
                name={"extract"}
                register={register}
                required={false}
              />
            </div>

            <div className="col-span-6 sm:col-span-6">
              <TextAreaCharacterForm
                label={"Descripción"}
                name={"description"}
                register={register}
                required={false}
              />
            </div>

            <div className="col-span-6 sm:col-span-1">
              <label className="block text-sm font-medium text-gray-700">
                Género
              </label>
              <select 
                {...register("gender")}
                className="py-2 px-3 rounded-lg border-2 border-purple-300 mt-1 focus:outline-none focus:ring-2 focus:ring-purple-600 focus:border-transparent"
              >
                <option value="male">Masculino</option>
                <option value="female">Femenino</option>
                <option value="other">otro</option>
              </select>
            </div>

            <div className="col-span-6 sm:col-span-1 flex items-end">
              <CheckBoxCharacterForm
                name={"active"}
                register={register}
                required={false}
              />
            </div>

            <div className="col-span-6 sm:col-span-6">
              <InputFile
                imageUrl={character.imageUrl}
                label={"Imagen"}
                required={false}
                setFile={setFile}
              />
            </div>
          </div>
        </div>
        <div className="px-4 py-3 bg-gray-50 text-right sm:px-6">
          <ButtonPrimary text="Guardar" type="submit" loading={loading} />
        </div>
      </div>
    </form>
  )
}

export default TitanForm
