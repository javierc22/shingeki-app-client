import { useContext } from 'react'
import TitanContext from "../../../../context/admin/titans/TitanContext"
import "./AlertSuccess.css"

type AlertSuccessType = {
  title?: string | undefined
  success: boolean
}

const AlertSuccess = ({title, success}: AlertSuccessType) => {
  const { setSuccess } = useContext(TitanContext)

  const handleSetSuccess = () => {
    setSuccess(false)
  }

  return (
    <div className={`${success ? "j-transition-in" : "j-transition-out"} alert-success`} role="alert">
      <span>{title}</span>
      <button className="w-4 ml-5 btn-outline-none" type="button" data-dismiss="alert" aria-label="Close" onClick={handleSetSuccess}>
        <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor">
          <path strokeLinecap="round" strokeLinejoin="round" strokeWidth={2} d="M6 18L18 6M6 6l12 12" />
        </svg>
      </button>
    </div>
  )
}

export default AlertSuccess
