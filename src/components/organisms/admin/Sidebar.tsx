import {useState} from 'react'
import SidebarIconSvg from '../../atoms/Icons/SidebarIconSvg'
import LinkSidebar from '../../molecules/LinkSidebar/LinkSidebar'

type SidebarProps = {
  sidebarOpen: boolean
}

const Sidebar = ({sidebarOpen}: SidebarProps) => {

  const [selectedMenu, setSelectedMenu] = useState<string>("/admin/index")

  return (
    <div
      className={`${
        sidebarOpen
          ? "translate-x-0 ease-out lg:static lg:inset-0"
          : "-translate-x-full ease-in"
      } fixed z-30 inset-y-0 left-0 w-64 transition duration-300 transform bg-gray-900 overflow-y-auto`}
    >
      <div className="flex items-center justify-center mt-8">
        <div className="flex items-center">
          <SidebarIconSvg />
          <span className="text-white text-2xl mx-2 font-semibold">
            Dashboard
          </span>
        </div>
      </div>

      <nav className="mt-10">

        <LinkSidebar 
          setSelectedMenu={setSelectedMenu}
          selectedMenu={selectedMenu}
          url="/admin"
          title="Dashboard"
          icon={"sidebar-icon-1"}
        />

        <LinkSidebar 
          setSelectedMenu={setSelectedMenu}
          selectedMenu={selectedMenu}
          url="/admin/titanes"
          title="Titanes"
          icon={"sidebar-icon-2"}
        />

        <LinkSidebar 
          setSelectedMenu={setSelectedMenu}
          selectedMenu={selectedMenu}
          url="/admin/personajes"
          title="Personajes"
          icon={"sidebar-icon-3"}
        />
      </nav>
    </div>
  )
}

export default Sidebar
