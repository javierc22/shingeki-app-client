import imgCharacterLevi from '../../assets/img/character-levi.jpg'

type CardCharacterProps = {
  name: string
}

const CardCharacter = ({name}: CardCharacterProps) => {
  return (
    <div className="shadow-lg rounded-2xl w-80 p-4 bg-white dark:bg-gray-800">
      <div className="flex flex-row items-start gap-4">
        <img src={imgCharacterLevi} alt="image_character" className="w-28 h-28 rounded-lg"/>
        <div className="h-28 w-full flex flex-col justify-between">
          <div>
            <p className="text-gray-800 dark:text-white text-xl font-medium">
              {name}
            </p>
          </div>
          <div className="rounded-lg dark:bg-white p-2 w-full">
            <div className="flex items-center justify-between text-xs text-gray-400 dark:text-black">
              <p className="flex flex-col">
                Sexo
                <span className="text-black dark:text-indigo-500 font-bold">
                  Masculino
                </span>
              </p>
              <p className="flex flex-col">
                Edad
                <span className="text-black dark:text-indigo-500 font-bold">
                  +30
                </span>
              </p>
            </div>
          </div>
        </div>
      </div>
    </div>
  )
}

export default CardCharacter
