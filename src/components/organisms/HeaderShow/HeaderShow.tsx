import useDateFormat from "../../../hooks/useDateFormat"

interface IHeaderShowProps {
  name: string
  userName?: string
  createdAt: string
}

const HeaderShow = (props: IHeaderShowProps) => {

  const { name, userName, createdAt } = props
  const { dateFormatted } = useDateFormat(createdAt)

  return (
    <div className="shadow overflow-hidden sm:rounded-md">
      <div className="px-4 py-5 bg-white sm:p-6">
        <h3 className="text-gray-700 text-3xl font-medium">
          { name }
        </h3>

        { userName && 
          (
            <h4 className="text-gray-700 text-2lg">
              Usuario Portador: { userName }
            </h4>
          )
        }

        <h4 className="text-gray-700 text-2lg">
          Fecha creación: { dateFormatted }
        </h4>
      </div>
    </div>
  )
}

export default HeaderShow
