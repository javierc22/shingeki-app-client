import React, {useState} from 'react'

import Logo from '../atoms/Logo/Logo'
import NavMenu from '../molecules/NavMenu/NavMenu'
import NavMenuMobile from '../molecules/NavMenuMobile/NavMenuMobile'
import ButtonMenuMobile from '../atoms/ButtonMenuMobile/ButtonMenuMobile'

export const Navbar = () => {

  const [openMenu, setOpenMenu] = useState(false)

  const handleToggleMenu = () => {
    setOpenMenu(!openMenu)
  }

  const handleCloseMenu = () => {
    setOpenMenu(false)
  }

  return (
    <>
      <div className="relative bg-white">
        <div className="max-w-7xl mx-auto px-4 sm:px-6 border-b-2 border-gray-100">
          <div className="flex justify-between items-center py-6 md:justify-start md:space-x-10">
            <div className="flex justify-start lg:w-0 lg:flex-1">
              <Logo />
            </div>
            <div className="-mr-2 -my-2 md:hidden">
              <ButtonMenuMobile handleToggleMenu={handleToggleMenu} />
            </div>

            <nav className="hidden md:flex space-x-10">
              <NavMenu />
            </nav>
          </div>
        </div>

        { openMenu && <NavMenuMobile handleCloseMenu={handleCloseMenu} />}
      </div>
    </>
  );
}
