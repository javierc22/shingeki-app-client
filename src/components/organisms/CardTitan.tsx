import titanEren from '../../assets/img/titan-eren.jpg'

type CardTitanProps = {
  name: string,
  description: string
}

const CardTitan = ({name, description}:CardTitanProps) => {
  return (
    <div className="flex max-w-md bg-white shadow-lg rounded-lg overflow-hidden">
      <div className="w-1/3 bg-cover bg-landscape">
        <img src={titanEren} alt="image_titan" />
      </div>
      <div className="w-2/3 p-4">
        <h1 className="text-gray-900 font-bold text-2xl">
          {name}
        </h1>
        <p className="mt-2 text-gray-600 text-sm">
          {description}
        </p>
        <div className="flex item-center mt-2">

        </div>
        <div className="flex item-center justify-between mt-3">
          <h1 className="text-gray-700 font-bold text-base">
            Altura: 220 mts
          </h1>
        </div>
      </div>
    </div>
  )
}

export default CardTitan
