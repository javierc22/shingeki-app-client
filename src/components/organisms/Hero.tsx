import React from 'react'
import HeroImage from '../../assets/img/hero-image.jpg'
import HeroBtnPrimary from '../atoms/HeroBtnPrimary/HeroBtnPrimary'
import HeroBtnSecondary from '../atoms/HeroBtnSecondary/HeroBtnSecondary'
import SubtitleHero from '../atoms/SubtitleHero/SubtitleHero'
import TitleHero from '../atoms/TitleHero/TitleHero'

const Hero = () => {
  return (
    <div className="bg-gray-100 py-10">
      <div className="container px-4 sm:px-8 lg:px-16 xl:px-20 mx-auto">
        <div className="grid grid-cols-1 md:grid-cols-12 gap-8 items-center">
          <div className="col-span-6">
            <TitleHero title={"Ataque a los Titanes"} />
            <SubtitleHero subtitle={"¿Qué personaje buscas?"}/>

            <div className="get-app flex space-x-5 mt-10 justify-center md:justify-start">
              <HeroBtnPrimary title={"Personajes"} />
              <HeroBtnSecondary title={"Titanes"} />
            </div>
          </div>

          <div className="col-span-6">
            <img src={HeroImage} alt="imagen" />
          </div>
        </div>
      </div>
    </div>
  )
}

export default Hero
