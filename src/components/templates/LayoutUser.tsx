import React from 'react'
import { Navbar } from '../organisms/Navbar'

type LayoutUserProps = {
  children: React.ReactNode
}

const LayoutUser = ({children}: LayoutUserProps) => {

  return (
    <div>
      <Navbar />
      <div>
        {children}
      </div>
    </div>
  )
}

export default LayoutUser
