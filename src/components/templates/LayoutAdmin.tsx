import {useState} from 'react'
import AdminNavbar from '../organisms/admin/AdminNavbar'
import Sidebar from '../organisms/admin/Sidebar'

type LayoutAdminProps = {
  children: React.ReactNode
}

const LayoutAdmin = ({children}: LayoutAdminProps) => {

  const [sidebarOpen, setSidebarOpen] = useState<boolean>(true)

  return (
    <div className="flex h-screen bg-gray-200">
      <div
        onClick={() => setSidebarOpen(false)}
        className={`${sidebarOpen ? "block" : "hidden"} fixed z-20 inset-0 bg-black opacity-50 transition-opacity lg:hidden`}
      ></div>

      <Sidebar sidebarOpen={sidebarOpen} />

      <div className="flex-1 flex flex-col overflow-hidden">
        <AdminNavbar sidebarOpen={sidebarOpen} setSidebarOpen={setSidebarOpen} />
          
        <main className="flex-1 overflow-x-hidden overflow-y-auto bg-gray-200">
          <div className="container mx-auto px-6 py-8">
            {children}
          </div>
        </main>
      </div>
    </div>
  )
}

export default LayoutAdmin
