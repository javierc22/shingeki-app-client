import { useEffect, useState, useContext, useCallback } from 'react'


import ButtonAddNew from '../../../components/atoms/ButtonAddNew/ButtonAddNew'
import Loader from '../../../components/atoms/Loader/Loader'
import DashboardTable from '../../../components/organisms/admin/DashboardTable/DashboardTable'
import ModalError from '../../../components/organisms/admin/ModalError/ModalError'
import PaginationButtons from '../../../components/organisms/admin/PaginationButtons/PaginationButtons'
import axiosClient from '../../../config/axiosClient'
import TitanContext from '../../../context/admin/titans/TitanContext'

const IndexTitans = () => {

  const { error, loading, fetchInit, fetchFinish, fetchError } = useContext(TitanContext)
  const [titans, setTitans] = useState([])
  const [page, setPage] = useState(1)
  const [paginationInfo, setPaginationInfo] = useState({
    totalPages: 0,
    totalDocs: 0,
  })

  const nextPage =() => setPage(page + 1)
  const previousPage =() => setPage(page - 1)

  const fetchTitans = useCallback(async() => {
    fetchInit()
    try {
      const { data } = await axiosClient.get(`/admin/titans?page=${page}`)
      setTitans(data.titans.docs)
      setPaginationInfo({
        totalPages: data.titans.totalPages, 
        totalDocs: data.titans.totalDocs
      })
      fetchFinish()
    } catch (error) {
      fetchError("")
    }
  }, [fetchInit, fetchFinish, fetchError, page])

  useEffect(() => {
    fetchTitans()
  }, [fetchTitans])

  if (error) return <ModalError error={error} />

  return (
    <>
      <h3 className="text-gray-700 text-3xl font-medium">
        Titanes
      </h3>
      
      <div className="mt-5"></div>

      <ButtonAddNew url={"/admin/titanes/nuevo"} />

      <div className="flex flex-col mt-2">
        <div className="-my-2 py-2 overflow-x-auto sm:-mx-6 sm:px-6 lg:-mx-8 lg:px-8">
            { loading ?
                <Loader />
              :
                (
                  <div className="align-middle inline-block min-w-full shadow overflow-hidden sm:rounded-lg border-b border-gray-200">
                    <DashboardTable items={titans} type={"titanes"} />
                    <PaginationButtons 
                      page={page} 
                      paginationInfo={paginationInfo}
                      nextPage={nextPage}
                      previousPage={previousPage}
                    />
                  </div>
                )
            }
        </div>
      </div>
    </>
  )
}

export default IndexTitans
