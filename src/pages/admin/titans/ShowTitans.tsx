import { useContext } from 'react'
import { useParams } from "react-router-dom"
import { Link } from 'react-router-dom'

import ModalError from '../../../components/organisms/admin/ModalError/ModalError'
import defaultImg from '../../../assets/img/default.jpg'
import { useDataApi } from "../../../hooks/useDataApi"
import ButtonSecondary from "../../../components/atoms/ButtonSecondary/ButtonSecondary"
import ButtonDanger from "../../../components/atoms/ButtonDanger/ButtonDanger"
import useAlertDelete from "../../../hooks/useAlertDelete"
import AlertSuccess from "../../../components/organisms/admin/AlertSuccess/AlertSuccess"
import TitanContext from "../../../context/admin/titans/TitanContext"
import Loader from '../../../components/atoms/Loader/Loader'
import ImageShow from '../../../components/atoms/ImageShow/ImageShow'
import HeaderShow from '../../../components/organisms/HeaderShow/HeaderShow'
import ContentShow from '../../../components/organisms/admin/ContentShow/ContentShow'

const ShowTitans = () => {
  let { id } = useParams<{ id: string }>()

  const { success, message } = useContext(TitanContext)

  const { data:titan, loading, error } = useDataApi(
    `/admin/titans/${id}`,
    {
      name: '',
      userName: '',
      height: 0,
      extract: '',
      description: '',
      imageUrl: '',
      createdAt: "",
    }
  )
  
  const { handleDelete } = useAlertDelete(
    '¿Estás seguro de eliminar este Titán?',
    `/admin/titans/${id}`
  )

  if (loading) return <Loader />

  if (error || !titan) return <ModalError error={error} />

  return (
    <>
      <div className="relative h-auto w-full">
        <div className="absolute -top-5 right-0 lg:w-2/5 sm:w-full">
          <AlertSuccess title={message} success={success} />
        </div>
      </div>

      <div className="grid md:grid-cols-4 sm:grid-cols-1">
        <div className="md:col-span-1 flex justify-center mb-5">
          <ImageShow imageSrc={titan.imageUrl ? titan.imageUrl : defaultImg} />
        </div>

        <div className="md:col-span-3">
          <HeaderShow name={titan.name} userName={titan.userName} createdAt={titan.createdAt} />

          <ContentShow extract={titan.extract} description={titan.description} />

          <div className="py-3 sm:flex sm:flex-row-reverse rounded-b-lg">
            <ButtonDanger type="button" text="Eliminar" handleOnClick={handleDelete} />

            <Link to={`./${id}/editar`}>
              <ButtonSecondary type="button" text="Editar" />
            </Link>
          </div>
        </div>
      </div>
    </>
  )
}

export default ShowTitans
