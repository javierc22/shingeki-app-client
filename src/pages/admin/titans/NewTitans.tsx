import { useState, useContext } from 'react'
import TitanForm from '../../../components/organisms/admin/TitanForm/TitanForm'
import TitanContext from '../../../context/admin/titans/TitanContext'
import AlertError from "../../../components/organisms/admin/AlertError/AlertError"

interface ITitan {
  name: string
  userName: string
  height: number
  extract: string
  description: string
  active: boolean
}

const NewTitans = () => {

  const { createTitan, error } = useContext(TitanContext)

  const [titan] = useState<ITitan>({
    name: '',
    userName: '',
    height: 0,
    extract: '',
    description: '',
    active: false
  })

  return (
    <>
      <div className="relative h-auto w-full">
        <div className="absolute -top-5 right-0 lg:w-2/5 sm:w-full">
          <AlertError error={error} />
        </div>
      </div>
      <div className="md:grid md:grid-cols-3 md:gap-6">
        <div className="md:col-span-3">
          <div className="px-4 sm:px-0">
            <h3 className="text-lg font-medium leading-6 text-gray-900">
              Crear Titán
            </h3>
            <p className="mt-1 text-sm text-gray-600">
              Ingresa los datos de tu nuevo elemento a crear.
            </p>
          </div>
        </div>

        <div className="mt-5 md:mt-0 md:col-span-3">
          <TitanForm titan={titan} submitForm={createTitan} />
        </div>
      </div>
    </>
  )
}

export default NewTitans
