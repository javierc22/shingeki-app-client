import { useContext, useEffect, useState, useCallback } from 'react'
import { useParams } from "react-router-dom"
import ModalError from '../../../components/organisms/admin/ModalError/ModalError'
import TitanForm from '../../../components/organisms/admin/TitanForm/TitanForm'
import TitanContext from '../../../context/admin/titans/TitanContext'
import axiosClient from '../../../config/axiosClient'
import { ITitan } from '../../../context/admin/titans/TitanState'
import AlertError from '../../../components/organisms/admin/AlertError/AlertError'
import Loader from '../../../components/atoms/Loader/Loader'


const EditTitans = () => {
  let { id } = useParams<{ id: string }>()

  const { error, loading, fetchInit, fetchFinish, fetchError } = useContext(TitanContext)
  const [titan, setTitan] = useState<ITitan>()

  const { updateTitan } = useContext(TitanContext)

  const fetchTitan = useCallback(async(id: string) => {
    fetchInit()
    try {
      const { data } = await axiosClient.get(`/admin/titans/${id}`)
      setTitan(data.data)
      fetchFinish()
    } catch (error) {
      fetchError("")
    }
  }, [fetchInit, fetchFinish, fetchError])

  useEffect(() => {
    fetchTitan(id)
  }, [id, fetchTitan])

  if (loading) return <Loader />

  if (!titan) return <ModalError error={error} />

  return (
    <>
      <div className="relative h-auto w-full">
        <div className="absolute -top-5 right-0 lg:w-2/5 sm:w-full">
          <AlertError error={error} />
        </div>
      </div>
      <div className="md:grid md:grid-cols-3 md:gap-6">
        <div className="md:col-span-3">
          <div className="px-4 sm:px-0">
            <h3 className="text-lg font-medium leading-6 text-gray-900">
              Editar Titán
            </h3>
            <p className="mt-1 text-sm text-gray-600">
              Ingresa los datos del Titán a editar
            </p>
          </div>
        </div>

        <div className="mt-5 md:mt-0 md:col-span-3">
          <TitanForm titan={titan} submitForm={updateTitan} />
        </div>
      </div>
    </>
  )
}

export default EditTitans
