import React, {useContext} from 'react'
import { useForm } from "react-hook-form"
import UserContext from '../../context/user/UserContext'

interface FormData {
  email: string
  password: string
}

const Login = () => {

  const { register, handleSubmit, formState: { errors } } = useForm<FormData>()

  const { loginUser }= useContext(UserContext)

  const onSubmit = handleSubmit((data) => {
    loginUser(data)
  })

  return (
    <div className="bg-grey-lighter min-h-screen flex flex-col">
      <div className="container max-w-sm mx-auto flex-1 flex flex-col items-center justify-center px-2">
        <div className="bg-white px-6 py-6 rounded shadow-md text-black w-full">
          <h1 className="mb-8 text-3xl text-center">Login</h1>
          <form onSubmit={onSubmit}>
            <input 
              type="text"
              className="block border border-grey-light w-full p-3 rounded mb-4"
              placeholder="Email"
              value="admin@email.cl"
              {...register("email", { required: true })}
            />

            {errors.email && <p>This is required.</p>}

            <input 
              type="password"
              className="block border border-grey-light w-full p-3 rounded mb-4"
              placeholder="Password" 
              value="admin1234"
              {...register("password", { required: true })}
            />

            <button
              type="submit"
              className="w-full text-center py-3 rounded bg-green-500 text-white hover:bg-green-600 focus:outline-none my-1"
            >
              Iniciar sesión
            </button>
          </form>
        </div>
      </div>
    </div>
  )
}

export default Login
