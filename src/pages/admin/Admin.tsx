import {useContext} from 'react'
import UserContext from '../../context/user/UserContext'

const Admin = () => {

  const { name, email } = useContext(UserContext)

  return (
    <div>
      Bienvenido Administrator! :v

      {name}
      {email}
    </div>
  )
}

export default Admin
