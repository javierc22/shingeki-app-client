import {useEffect, useContext, useState, useCallback} from 'react'
import { useParams, useHistory } from "react-router-dom"

import Loader from "../../../components/atoms/Loader/Loader"
import AlertError from '../../../components/organisms/admin/AlertError/AlertError'
import CharacterForm from '../../../components/organisms/admin/CharacterForm/CharacterForm'
import ModalError from "../../../components/organisms/admin/ModalError/ModalError"
import axiosClient from '../../../config/axiosClient'
import CharacterContext from '../../../context/admin/characters/CharacterContext'
import { ICharacter } from '../../../context/admin/characters/CharacterState'

const EditCharacters = () => {
  let { id } = useParams<{ id: string }>()
  let history = useHistory()

  const { error, loading, fetchInit, fetchFinish, fetchError, fetchSuccess } = useContext(CharacterContext)
  const [character, setCharacter] = useState<ICharacter>()

  const updateCharacter = async(payload?:any) => {
    fetchInit()
    try {
      const formData = new FormData()
      formData.append('name', payload.name)
      formData.append('lastname', payload.lastname)
      formData.append('gender', payload.gender)
      formData.append('extract', payload.extract)
      formData.append('description', payload.description)
      formData.append('active', payload.active)
      formData.append('image', payload.image)

      await axiosClient.put(`/admin/characters/${payload.id}`, formData)
      history.push(`/admin/personajes/${payload.id}`)
      fetchSuccess("Personaje actualizado correctamente")
    } catch (error) {
      fetchError("")
    }
  }

  const fetchCharacter = useCallback(async(id: string) => {
    fetchInit()
    try {
      const { data } = await axiosClient.get(`/admin/characters/${id}`)
      setCharacter(data.data)
      fetchFinish()
    } catch (error) {
      fetchError("")
    }
  }, [fetchInit, fetchFinish, fetchError])

  useEffect(() => {
    fetchCharacter(id)
  }, [id, fetchCharacter])

  if (loading) return <Loader />

  if (!character) return <ModalError error={error} />

  return (
    <>
      <div className="relative h-auto w-full">
        <div className="absolute -top-5 right-0 lg:w-2/5 sm:w-full">
          <AlertError error={error} />
        </div>
      </div>
      <div className="md:grid md:grid-cols-3 md:gap-6">
        <div className="md:col-span-3">
          <div className="px-4 sm:px-0">
            <h3 className="text-lg font-medium leading-6 text-gray-900">
              Editar Personaje
            </h3>
            <p className="mt-1 text-sm text-gray-600">
              Ingresa los datos del personaje a editar
            </p>
          </div>
        </div>

        <div className="mt-5 md:mt-0 md:col-span-3">
          <CharacterForm character={character} submitForm={updateCharacter} />
        </div>
      </div>
    </>
  )
}

export default EditCharacters
