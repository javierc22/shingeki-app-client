import { useContext } from 'react'
import { useParams } from "react-router-dom"
import { Link } from 'react-router-dom'

import ModalError from '../../../components/organisms/admin/ModalError/ModalError'
import defaultImg from '../../../assets/img/default.jpg'
import { useDataApi } from "../../../hooks/useDataApi"
import ButtonSecondary from "../../../components/atoms/ButtonSecondary/ButtonSecondary"
import ButtonDanger from "../../../components/atoms/ButtonDanger/ButtonDanger"
import useAlertDelete from "../../../hooks/useAlertDelete"
import AlertSuccess from "../../../components/organisms/admin/AlertSuccess/AlertSuccess"
import CharacterContext from "../../../context/admin/characters/CharacterContext"
import Loader from '../../../components/atoms/Loader/Loader'
import ImageShow from '../../../components/atoms/ImageShow/ImageShow'
import HeaderShow from '../../../components/organisms/HeaderShow/HeaderShow'
import ContentShow from '../../../components/organisms/admin/ContentShow/ContentShow'

const ShowCharacter = () => {
  let { id } = useParams<{ id: string }>()

  const { success, message } = useContext(CharacterContext)

  const { data:character, loading, error } = useDataApi(
    `/admin/characters/${id}`,
    {
      name: '',
      lastname: '',
      gender: '',
      extract: '',
      description: '',
      imageUrl: '',
      createdAt: "",
    }
  )
  
  const { handleDelete } = useAlertDelete(
    '¿Estás seguro de eliminar este Titán?',
    `/admin/titans/${id}`
  )

  if (loading) return <Loader />

  if (error || !character) return <ModalError error={error} />

  return (
    <>
      <div className="relative h-auto w-full">
        <div className="absolute -top-5 right-0 lg:w-2/5 sm:w-full">
          <AlertSuccess title={message} success={success} />
        </div>
      </div>

      <div className="grid md:grid-cols-4 sm:grid-cols-1">
        <div className="md:col-span-1 flex justify-center mb-5">
          <ImageShow imageSrc={character.imageUrl ? character.imageUrl : defaultImg} />
        </div>

        <div className="md:col-span-3">
          <HeaderShow 
            name={`${character.name} ${character.lastname}`}
            createdAt={character.createdAt} 
          />

          <ContentShow extract={character.extract} description={character.description} />

          <div className="py-3 sm:flex sm:flex-row-reverse rounded-b-lg">
            <ButtonDanger type="button" text="Eliminar" handleOnClick={handleDelete} />

            <Link to={`./${id}/editar`}>
              <ButtonSecondary type="button" text="Editar" />
            </Link>
          </div>
        </div>
      </div>
    </>
  )
}

export default ShowCharacter
