import { useState, useContext, useCallback, useEffect } from 'react'
import ButtonAddNew from '../../../components/atoms/ButtonAddNew/ButtonAddNew'
import Loader from '../../../components/atoms/Loader/Loader'
import DashboardTable from '../../../components/organisms/admin/DashboardTable/DashboardTable'
import ModalError from '../../../components/organisms/admin/ModalError/ModalError'
import PaginationButtons from '../../../components/organisms/admin/PaginationButtons/PaginationButtons'
import axiosClient from '../../../config/axiosClient'
import CharacterContext from "../../../context/admin/characters/CharacterContext"

const IndexCharacters = () => {

  const { error, loading, fetchInit, fetchFinish, fetchError } = useContext(CharacterContext)
  const [characters, setCharacters] = useState([])
  const [page, setPage] = useState(1)
  const [paginationInfo, setPaginationInfo] = useState({
    totalPages: 0,
    totalDocs: 0,
  })

  const nextPage =() => setPage(page + 1)
  const previousPage =() => setPage(page - 1)

  const fetchCharacters = useCallback(async() => {
    fetchInit()
    try {
      const { data } = await axiosClient.get(`/admin/characters?page=${page}`)
      setCharacters(data.characters.docs)
      setPaginationInfo({
        totalPages: data.characters.totalPages, 
        totalDocs: data.characters.totalDocs
      })
      fetchFinish()
    } catch (error) {
      fetchError("")
    }
  }, [fetchInit, fetchFinish, fetchError, page])

  useEffect(() => {
    fetchCharacters()
  }, [fetchCharacters])

  if (error) return <ModalError error={error} />

  return (
    <>
      <h3 className="text-gray-700 text-3xl font-medium">
        Personajes
      </h3>
      
      <div className="mt-5"></div>

      <ButtonAddNew url={"/admin/personajes/nuevo"} />

      <div className="flex flex-col mt-2">
        <div className="-my-2 py-2 overflow-x-auto sm:-mx-6 sm:px-6 lg:-mx-8 lg:px-8">
            { loading ?
                <Loader />
              :
                (
                  <div className="align-middle inline-block min-w-full shadow overflow-hidden sm:rounded-lg border-b border-gray-200">
                    <DashboardTable items={characters} type={"personajes"} />
                    <PaginationButtons 
                      page={page} 
                      paginationInfo={paginationInfo}
                      nextPage={nextPage}
                      previousPage={previousPage}
                    />
                  </div>
                )
            }
        </div>
      </div>
    </>
  )
}

export default IndexCharacters
