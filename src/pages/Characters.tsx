import {useState, useEffect} from 'react'
import Axios from 'axios'

import CardCharacter from '../components/organisms/CardCharacter'

interface Character {
  _id: string,
  name: string,
  age: string,
  gender: string
}

const Characters = () => {
  const [characters, setCharacters] = useState<Character[]>([])

  useEffect(() => {
    fetchCharacters()
  }, [])

  const fetchCharacters = () => {
    const url = "http://localhost:4000/api/v1/characters"
    Axios.get(url)
      .then( response => {
        setCharacters(response.data.characters)
      })
      .catch( err => {
        console.log(err)
      })
  }

  return (
    <div className="container mx-auto">
      <div className="grid grid-cols-1 sm:grid-cols-2 md:grid-cols-3 lg:grid-cols-3">
        {
          characters.map( character => (
            <div key={character._id} className="flex justify-center my-4">
              <CardCharacter name={character.name}/>
            </div>
          ))
        }
      </div>
    </div>
  )
}

export default Characters
