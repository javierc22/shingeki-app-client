import React from 'react'
import Hero from '../components/organisms/Hero';

const Home = () => {
  return (
    <Hero />
  );
}

export default Home
