import {useState, useEffect} from 'react'
import CardTitan from '../components/organisms/CardTitan'
import Axios from "axios"

interface Titan {
  id: number,
  name: string,
  extract: string,
  height: number,
  imageUrl: string,
  description: string
}

const Titans = () => {
  const [titans, setTitans] = useState<Titan[]>([])

  const fetchTitans = () => {
    const url = "http://localhost:4000/api/v1/titans"
    Axios.get(url)
      .then( response => {
        setTitans(response.data.titans)
      })
      .catch( err => {
        console.log(err)
      })
  }

  useEffect(() => {
    fetchTitans()
  }, [])

  return (
    <div className="container mx-auto">
      <div className="grid grid-cols-2">
        {
          titans.map( titan => (
            <div key={titan.id} className="flex justify-center my-4">
              <CardTitan name={titan.name} description={titan.extract} />
            </div>
          ))
        }
      </div>
    </div>
  )
}

export default Titans
