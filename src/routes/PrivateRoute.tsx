import { Route, RouteProps, Redirect } from "react-router-dom"
import LayoutAdmin from '../components/templates/LayoutAdmin'

interface AdminRouterProps extends RouteProps{
  component: any;
  path: string;
  exact: boolean;
  isAuthenticated: boolean;
}

const PrivateRoute = (props: AdminRouterProps) => {
  const { component: Component, isAuthenticated, ...rest } = props

  return (
    <Route {...rest} render={ routeProps => (
      isAuthenticated ?
        <LayoutAdmin>
          <Component {...routeProps} />
        </LayoutAdmin>
      : 
        <Redirect to="/admin/login" />
    )} />
  )
}

export default PrivateRoute
