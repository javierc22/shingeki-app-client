import {BrowserRouter as Router, Switch, Route} from "react-router-dom"

import UserRoute from "./UserRoute"
import AdminRouter from "./AdminRouter"

import Characters from "../pages/Characters"
import Curiosities from "../pages/Curiosities"
import Home from '../pages/Home'
import Titans from "../pages/Titans"

const AppRouter = () => {
  return (
    <Router>
      <Switch>
        <UserRoute path="/" exact component={Home} />
        <UserRoute path="/personajes" exact component={Characters} />
        <UserRoute path="/titanes" exact component={Titans} />
        <UserRoute path="/curiosidades" exact component={Curiosities} />

        <AdminRouter />
        
        <Route path="*">
          <h3>No Existe</h3>
        </Route>
      </Switch>
    </Router>
  )
}

export default AppRouter
