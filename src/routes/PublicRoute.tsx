import { Route, RouteProps, Redirect } from "react-router-dom"

interface PublicRouteProps extends RouteProps{
  component: any;
  path: string;
  exact: boolean;
  isAuthenticated: boolean;
}

const PublicRoute = (props: PublicRouteProps) => {

  const { component: Component, isAuthenticated, ...rest } = props

  return (
    <Route {...rest} render={ routeProps => (
      isAuthenticated ?
        <Redirect to="/admin" />
      :
        <Component {...routeProps} />
    )} />
  )
}

export default PublicRoute
