import { useContext, useEffect } from 'react'
import {BrowserRouter as Router, Switch, Route} from "react-router-dom"

import PrivateRoute from "./PrivateRoute"
import PublicRoute from "./PublicRoute"
import Admin from "../pages/admin/Admin"
import Login from "../pages/admin/Login"
import UserContext from '../context/user/UserContext'
import IndexTitans from '../pages/admin/titans/IndexTitans'
import Characters from '../pages/admin/Characters'
import TitanProvider from '../context/admin/titans/TitanProvider'
import NewTitans from '../pages/admin/titans/NewTitans'
import ShowTitans from '../pages/admin/titans/ShowTitans'
import EditTitans from '../pages/admin/titans/EditTitans'
import IndexCharacters from '../pages/admin/characters/IndexCharacters'
import NewCharacters from '../pages/admin/characters/NewCharacters'
import ShowCharacters from '../pages/admin/characters/ShowCharacters'
import EditCharacters from '../pages/admin/characters/EditCharacters'

const AdminRouter = () => {

  const { authenticateUser, authenticated, loading } = useContext(UserContext)

  useEffect(() => {
    authenticateUser()
  }, [authenticateUser])

  if (loading) return (<h2>Espere...</h2>)

  return (
    <Router>
      <TitanProvider>
        <Switch>
          <PrivateRoute path="/admin" exact component={Admin} isAuthenticated={authenticated} />
          <PublicRoute path="/admin/login" exact component={Login} isAuthenticated={authenticated} />

          <PrivateRoute path="/admin/titanes" exact component={IndexTitans} isAuthenticated={authenticated} />
          <PrivateRoute path="/admin/titanes/nuevo" exact component={NewTitans} isAuthenticated={authenticated} />
          <PrivateRoute path="/admin/titanes/:id" exact component={ShowTitans} isAuthenticated={authenticated} />
          <PrivateRoute path="/admin/titanes/:id/editar" exact component={EditTitans} isAuthenticated={authenticated} />

          <PrivateRoute path="/admin/personajes" exact component={IndexCharacters} isAuthenticated={authenticated} />
          <PrivateRoute path="/admin/personajes/nuevo" exact component={NewCharacters} isAuthenticated={authenticated} />
          <PrivateRoute path="/admin/personajes/:id" exact component={ShowCharacters} isAuthenticated={authenticated} />
          <PrivateRoute path="/admin/personajes/:id/editar" exact component={EditCharacters} isAuthenticated={authenticated} />

          <Route path="*">
            <h3>No Existe</h3>
          </Route>
        </Switch>
      </TitanProvider>
    </Router>
  )
}

export default AdminRouter
