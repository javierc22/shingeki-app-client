import { Route, RouteProps } from "react-router-dom"
import LayoutUser from '../components/templates/LayoutUser'

interface AdminRouterProps extends RouteProps{
  component: any;
  path: string;
  exact: boolean;
}

const UserRoute = (props: AdminRouterProps) => {

  const { component: Component, ...rest } = props

  return (
    <Route {...rest} render={ routeProps => (
      <LayoutUser>
        <Component {...routeProps} />
      </LayoutUser>
    )} />
  )
}

export default UserRoute
