import { createContext } from 'react'
import { titanInitialState } from './TitanState'

const TitanContext = createContext(titanInitialState)

export default TitanContext
