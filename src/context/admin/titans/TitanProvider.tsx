import { useReducer, useCallback } from 'react'
import { useHistory } from "react-router-dom"
import { titanInitialState } from "./TitanState"
import TitanContext from "./TitanContext"
import axiosClient from "../../../config/axiosClient"
import titanReducer from './titanReducer'
import { types } from './titanTypes'

const TitanProvider: React.FC = (props) => {
  let history = useHistory();
  const [state, dispatch] = useReducer(titanReducer, titanInitialState)

  const createTitan = async(payload:any) => {
    fetchInit()
    try {
      const formData = new FormData()
      formData.append('name', payload.name)
      formData.append('userName', payload.userName)
      formData.append('height', payload.height)
      formData.append('extract', payload.extract)
      formData.append('description', payload.description)
      formData.append('active', payload.active)
      formData.append('image', payload.image)

      const {data} = await axiosClient.post('/admin/titans', formData)
      history.push(`/admin/titanes/${data.id}`)
      fetchSuccess("Titán creado correctamente")
    } catch (error) {
      fetchError("")
    }
  }

  const updateTitan = async(payload?:any) => {
    fetchInit()
    try {
      const formData = new FormData()
      formData.append('name', payload.name)
      formData.append('userName', payload.userName)
      formData.append('height', payload.height)
      formData.append('extract', payload.extract)
      formData.append('description', payload.description)
      formData.append('active', payload.active)
      formData.append('image', payload.image)

      await axiosClient.put(`/admin/titans/${payload.id}`, formData)
      history.push(`/admin/titanes/${payload.id}`)
      fetchSuccess("Titán actualizado correctamente")
    } catch (error) {
      fetchError("")
    }
  }

  const setSuccess = (value: boolean, msg: string = '') => {
    dispatch({ type: types.CLEAR_SUCCESS })
    dispatch({ type: types.SET_SUCCESS, payload: value })
    dispatch({ type: types.SET_MESSAGE, payload: msg })

    setTimeout(() => {
      dispatch({ type: types.CLEAR_SUCCESS })
    }, 3000)
  }

  const fetchInit = useCallback(() => {
    dispatch({ type: types.FETCH_INIT })
  }, [])

  const fetchSuccess =  useCallback((msg: string = "") => {
    dispatch({ type: types.FETCH_SUCCESS, payload: msg })

    setTimeout(() => {
      dispatch({ type: types.CLEAR_SUCCESS })
    }, 3000)
  }, [])

  const fetchError =  useCallback((msg: string = "") => {
    dispatch({ type: types.FETCH_ERROR, payload: msg })
  }, [])

  const fetchFinish =  useCallback(() => {
    dispatch({ type: types.FETCH_FINISH })
  }, [])

  return (
    <TitanContext.Provider 
      value={{
        loading: state.loading,
        error: state.error,
        success: state.success,
        message: state.message,
        setSuccess,
        createTitan,
        updateTitan,
        fetchInit,
        fetchSuccess,
        fetchError,
        fetchFinish
      }}
    >
      {props.children}
    </TitanContext.Provider>
  )
}

export default TitanProvider
