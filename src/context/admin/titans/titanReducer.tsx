import { ITitanState } from './TitanState'
import { types } from './titanTypes'
import { ActionType } from './titanTypes'

const titanReducer = (state: ITitanState, action: ActionType) => {
  switch (action.type) {
    case types.SET_SUCCESS:
      return {
        ...state, 
        success: action.payload,
        error: false
      }

    case types.SET_MESSAGE:
      return {
        ...state, 
        message: action.payload
      }

    case types.CLEAR_SUCCESS:
      return {
        ...state, 
        success: false,
        message: ''
      }
    
    case types.CLEAN_ERROR:
      return {
        ...state,
        error: false,
        message: ''
      }

    case types.SET_ERROR:
      return {
        ...state,
        error: true,
        message: action.payload
      }

    case types.SET_LOADING:
      return {
        ...state,
        titans: [],
        titan: null,
        loading: action.payload,
      }

    case types.FETCH_ERROR:
      return {
        ...state,
        loading: false,
        success: false,
        error: true,
        message: action.payload
      }

    case types.FETCH_SUCCESS:
      return {
        ...state,
        loading: false,
        success: true,
        error: false,
        message: action.payload
      }

    case types.FETCH_INIT:
      return {
        ...state,
        loading: true,
        success: false,
        error: false,
        message: ''
      }
    
    case types.FETCH_FINISH:
      return {
        ...state,
        loading: false,
        success: false,
        error: false,
        message: ''
      }

    default:
      return state
  }

}

export default titanReducer
