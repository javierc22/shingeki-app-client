export const titanInitialState: ITitanState = {
  loading: false,
  error: false,
  message: '',
  success: false,
  setSuccess: () => {},
  createTitan: () => {},
  updateTitan: () => {},
  fetchInit: () => {},
  fetchSuccess: () => {},
  fetchError: () => {},
  fetchFinish: () => {}
}

export interface ITitanState {
  loading: boolean
  error: boolean
  message?: string | undefined
  success: boolean
  setSuccess: (value: boolean) => void
  createTitan: (payload: ITitan) => void
  updateTitan: (payload: ITitan) => void
  fetchInit: () => void
  fetchFinish: () => void
  fetchSuccess: (msg: string | undefined) => void
  fetchError: (msg: string | undefined) => void
}

export interface ITitan {
  id?: string
  name: string
  userName: string
  height: number
  extract: string
  description: string
  image?: File
  imageUrl?: string,
  active: boolean
}