import { ITitan } from './TitanState';

export enum types {
  GET_TITANS = 'GET_TITANS',
  CLEAR_SUCCESS = 'CLEAR_SUCCESS',
  SET_SUCCESS = 'SET_SUCCESS',
  CLEAN_ERROR = 'CLEAN_ERROR',
  SET_ERROR = 'SET_ERROR',
  FETCH_TITAN = 'FETCH_TITAN',
  SET_LOADING = 'SET_LOADING',
  SET_MESSAGE = 'SET_MESSAGE',
  FETCH_INIT = 'FETCH_INIT',
  FETCH_SUCCESS = 'FETCH_SUCCESS',
  FETCH_ERROR = 'FETCH_ERROR',
  FETCH_FINISH = 'FETCH_FINISH'
}

export type ActionType = 
  | { type: types.GET_TITANS, payload: ITitan[] }
  | { type: types.FETCH_TITAN, payload: ITitan }
  | { type: types.CLEAR_SUCCESS }
  | { type: types.SET_SUCCESS, payload: boolean }
  | { type: types.CLEAN_ERROR }
  | { type: types.SET_ERROR, payload: string }
  | { type: types.SET_LOADING, payload: boolean }
  | { type: types.SET_MESSAGE, payload?: string }
  | { type: types.FETCH_INIT }
  | { type: types.FETCH_SUCCESS, payload?: string }
  | { type: types.FETCH_ERROR, payload?: string }
  | { type: types.FETCH_FINISH }