import { createContext } from 'react'
import { initialState } from './CharacterState'

const CharacterContext = createContext(initialState)

export default CharacterContext