export enum types {
  SET_SUCCESS = 'SET_SUCCESS',
  CLEAR_SUCCESS = 'CLEAR_SUCCESS',
  FETCH_ERROR = 'FETCH_ERROR',
  FETCH_SUCCESS = 'FETCH_SUCCESS',
  FETCH_INIT = 'FETCH_INIT',
  FETCH_FINISH = 'FETCH_FINISH'
}

export type ActionType = 
  | { type: types.SET_SUCCESS, payload: boolean }
  | { type: types.CLEAR_SUCCESS }
  | { type: types.FETCH_ERROR, payload?: string }
  | { type: types.FETCH_SUCCESS, payload?: string }
  | { type: types.FETCH_INIT }
  | { type: types.FETCH_FINISH }
