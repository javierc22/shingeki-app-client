import { ICharacterState } from './CharacterState'
import { types } from './characterTypes'
import { ActionType } from './characterTypes'

const characterReducer = (state: ICharacterState, action: ActionType) => {
  switch (action.type) {
    case types.SET_SUCCESS:
      return {
        ...state, 
        success: action.payload,
        error: false
      }

    case types.CLEAR_SUCCESS:
      return {
        ...state, 
        success: false,
        message: ''
      }

    case types.FETCH_ERROR:
      return {
        ...state,
        loading: false,
        success: false,
        error: true,
        message: action.payload
      }

    case types.FETCH_SUCCESS:
      return {
        ...state,
        loading: false,
        success: true,
        error: false,
        message: action.payload
      }

    case types.FETCH_INIT:
      return {
        ...state,
        loading: true,
        success: false,
        error: false,
        message: ''
      }
    
    case types.FETCH_FINISH:
      return {
        ...state,
        loading: false,
        success: false,
        error: false,
        message: ''
    }

    default:
      return state
  }

}

export default characterReducer
