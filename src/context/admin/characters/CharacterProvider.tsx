import { useReducer, useCallback } from 'react'
import { useHistory } from "react-router-dom"
import { initialState } from "./CharacterState"
import CharacterContext from "./CharacterContext"
import axiosClient from "../../../config/axiosClient"
import characterReducer from './characterReducer'
import { types } from './characterTypes'

const CharacterProvider: React.FC = (props) => {
  let history = useHistory();
  const [state, dispatch] = useReducer(characterReducer, initialState)

  const createCharacter = async(payload:any) => {
    fetchInit()
    try {
      const formData = new FormData()
      formData.append('name', payload.name)
      formData.append('lastname', payload.lastname)
      formData.append('gender', payload.gender)
      formData.append('extract', payload.extract)
      formData.append('description', payload.description)
      formData.append('active', payload.active)
      formData.append('image', payload.image)

      const {data} = await axiosClient.post('/admin/characters', formData)
      history.push(`/admin/personajes/${data.id}`)
      fetchSuccess("Personaje creado correctamente")
    } catch (error) {
      fetchError("")
    }
  }

  const updateCharacter = async(payload?:any) => {
    console.log("En provider")
    fetchInit()
    try {
      const formData = new FormData()
      formData.append('name', payload.name)
      formData.append('lastname', payload.lastname)
      formData.append('gender', payload.gender)
      formData.append('extract', payload.extract)
      formData.append('description', payload.description)
      formData.append('active', payload.active)
      formData.append('image', payload.image)

      await axiosClient.put(`/admin/characters/${payload.id}`, formData)
      history.push(`/admin/personajes/${payload.id}`)
      fetchSuccess("Personaje actualizado correctamente")
    } catch (error) {
      fetchError("")
    }
  }

  const setSuccess = (value: boolean, msg: string = '') => {
    dispatch({ type: types.CLEAR_SUCCESS })
    dispatch({ type: types.SET_SUCCESS, payload: value })

    setTimeout(() => {
      dispatch({ type: types.CLEAR_SUCCESS })
    }, 3000)
  }

  const fetchInit = useCallback(() => {
    dispatch({ type: types.FETCH_INIT })
  }, [])

  const fetchSuccess =  useCallback((msg: string = "") => {
    dispatch({ type: types.FETCH_SUCCESS, payload: msg })

    setTimeout(() => {
      dispatch({ type: types.CLEAR_SUCCESS })
    }, 3000)
  }, [])

  const fetchError =  useCallback((msg: string = "") => {
    dispatch({ type: types.FETCH_ERROR, payload: msg })
  }, [])

  const fetchFinish =  useCallback(() => {
    dispatch({ type: types.FETCH_FINISH })
  }, [])

  return (
    <CharacterContext.Provider 
      value={{
        loading: state.loading,
        error: state.error,
        success: state.success,
        message: state.message,
        setSuccess,
        createCharacter,
        updateCharacter,
        fetchInit,
        fetchSuccess,
        fetchError,
        fetchFinish
      }}
    >
      {props.children}
    </CharacterContext.Provider>
  )
}

export default CharacterProvider
