export const initialState: ICharacterState = {
  loading: false,
  error: false,
  success: false,
  message: '',
  setSuccess: () => {},
  createCharacter: () => {},
  updateCharacter: () => {},
  fetchInit: () => {},
  fetchSuccess: () => {},
  fetchError: () => {},
  fetchFinish: () => {}
}

export interface ICharacterState {
  loading: boolean
  error: boolean
  success: boolean
  message?: string
  setSuccess: (value: boolean) => void
  createCharacter: (payload: ICharacter) => void
  updateCharacter: (payload: ICharacter) => void
  fetchInit: () => void
  fetchFinish: () => void
  fetchSuccess: (msg: string | undefined) => void
  fetchError: (msg: string | undefined) => void
}

export interface ICharacter {
  id?: string
  name: string
  lastname: string
  gender: string
  extract: string
  description: string
  image?: File
  imageUrl?: string,
  active: boolean
}