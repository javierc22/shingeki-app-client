type InitialStateType = {
  uid: string | null
  name: string | null
  email: string | null
  authenticated: boolean
  token: string | null
  loading: boolean
  loginUser: (payload:LoginUserType) => void
  authenticateUser: () => void
}

type LoginUserType = {
  email: string
  password: string
}

export const initialState:InitialStateType = {
  uid: null,
  name: null,
  email: null,
  authenticated: false,
  token: localStorage.getItem('token'),
  loading: true,
  loginUser: () => {},
  authenticateUser: () => {}
}
