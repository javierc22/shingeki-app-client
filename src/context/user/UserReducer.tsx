import { types } from './types'
import { ActionType } from "./ActionType"

type State = {
  uid: string | null
  name: string | null
  email: string | null
  authenticated: boolean
  token: string | null
  loading: boolean
}

const UserReducer = (state: State, action: ActionType) => {
  
  switch (action.type) {
    case types.LOGIN_USER:
      localStorage.setItem('token', action.payload.token);
      return {
        ...state,
        authenticated: true,
        loading: false
      }

    case types.GET_USER:
      return {
        ...state,
        uid: action.payload._id,
        name: action.payload.name,
        email: action.payload.email,
        authenticated: true,
        loading: false
      }

    case types.ERROR:
      return {
        ...state, loading: false
      }

    case types.SET_LOADING:
      return { ...state, loading: false }
  
    default:
      return state
  }
}

export default UserReducer
