import React, {useReducer, useCallback} from 'react'
import { types } from './types'
import axiosClient from '../../config/axiosClient'
import setTokenHeaders from '../../config/setTokenHeaders'
import UserContext from './UserContext'
import UserReducer from './UserReducer'

const initialState = {
  uid: null,
  name: null,
  email: null,
  authenticated: false,
  token: localStorage.getItem('token'),
  loading: true,
}

type LoginUserType = {
  email: string
  password: string
}

const UserProvider: React.FC = (props) => {

  const [state, dispatch] = useReducer(UserReducer, initialState)

  const loginUser = async(payload:LoginUserType) => {
    const url = "/admin/sign_in"
    try {
      const { data } = await axiosClient.post(url, payload)
      dispatch({ type: types.LOGIN_USER, payload: data })

      authenticateUser()
    } catch (error) {
      dispatch({ type: types.ERROR })
    }
  }

  const authenticateUser = useCallback(async() => {
    const token = localStorage.getItem('token') || ''

    if (!token) {
      dispatch({ type: types.SET_LOADING, payload: false })
      return
    }
    
    setTokenHeaders(token)

    try {
      const { data } = await axiosClient.get('/admin/auth')
      dispatch({ type: types.GET_USER, payload: data.user })
    } catch (error) {
      dispatch({ type: types.ERROR })
    }
  }, [])

  return (
    <UserContext.Provider value={{
      uid: state.uid,
      name: state.name,
      email: state.email,
      token: state.token,
      authenticated: state.authenticated,
      loading: state.loading,
      loginUser,
      authenticateUser
    }}>
      {props.children}
    </UserContext.Provider>
  )
}

export default UserProvider
