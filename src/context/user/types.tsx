export enum types {
  LOGIN_USER = "LOGIN_USER",
  GET_USER = "GET_USER",
  ERROR = "ERROR",
  SET_LOADING = "SET_LOADING"
}