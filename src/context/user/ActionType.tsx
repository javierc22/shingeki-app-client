import { types } from './types'

export type ActionType = 
  | { type: types.LOGIN_USER, payload: LoginPayload }
  | { type: types.GET_USER, payload: GetUserPayload }
  | { type: types.SET_LOADING, payload: boolean }
  | { type: types.ERROR }

type LoginPayload = {
  name: string,
  token: string
}

type GetUserPayload = {
  _id: string
  email: string,
  name: string
}