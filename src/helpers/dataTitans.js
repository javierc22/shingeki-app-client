const dataTitans = [
  {
    id: 1,
    name: 'Titán de Ataque',
    description: "You can't buy your future, but you can do it. Money is nothing, you'r everything. Lorem ipsum dolor sit amet consectetur, adipisicing elit. Accusantium, alias."
  },
  {
    id: 2,
    name: 'Titán Hembra',
    description: "You can't buy your future, but you can do it. Money is nothing, you'r everything. Lorem ipsum dolor sit amet consectetur, adipisicing elit. Accusantium, alias."
  },
  {
    id: 3,
    name: 'Titán Blindado',
    description: "You can't buy your future, but you can do it. Money is nothing, you'r everything. Lorem ipsum dolor sit amet consectetur, adipisicing elit. Accusantium, alias."
  },
  {
    id: 4,
    name: 'Titán Colosal',
    description: "You can't buy your future, but you can do it. Money is nothing, you'r everything. Lorem ipsum dolor sit amet consectetur, adipisicing elit. Accusantium, alias."
  },
  {
    id: 5,
    name: 'Titán Mandíbula',
    description: "You can't buy your future, but you can do it. Money is nothing, you'r everything. Lorem ipsum dolor sit amet consectetur, adipisicing elit. Accusantium, alias."
  },
  {
    id: 6,
    name: 'Titán Bestia',
    description: "You can't buy your future, but you can do it. Money is nothing, you'r everything. Lorem ipsum dolor sit amet consectetur, adipisicing elit. Accusantium, alias."
  }
]

export default dataTitans
