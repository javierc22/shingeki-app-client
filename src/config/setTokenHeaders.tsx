import axiosClient from "./axiosClient"

const setTokenHeaders = (token: String | '') => {
  if (token) {
    axiosClient.defaults.headers.common['x-auth-token'] = token
  } else {
    delete axiosClient.defaults.headers.common['x-auth-token']
  }
}

export default setTokenHeaders