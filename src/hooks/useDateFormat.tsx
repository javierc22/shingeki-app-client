import { DateTime } from "luxon"

const useDateFormat = (date: string) => {
  const dateFormatted = DateTime.fromISO(date).setLocale('es-CL').toLocaleString(DateTime.DATETIME_SHORT)

  return { dateFormatted }
}

export default useDateFormat
