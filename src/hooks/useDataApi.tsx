import {useState, useEffect, useCallback} from 'react'
import axiosClient from '../config/axiosClient'

export const useDataApi = <T extends Object>( url:string, initialData: T) => {
  const [data, setData] = useState(initialData)
  const [loading, setLoading] = useState<Boolean>(false)
  const [error, setError] = useState(false)

  const fetchData = useCallback(async() => {
    setLoading(true)
    setError(false)
    try {
      const { data } = await axiosClient.get(url)
      setData(data.data)
    } catch (error) {
      setError(true)
    }
    
    setLoading(false)
  }, [url])

  useEffect(() => {
    fetchData()
  }, [fetchData])


  return { data, loading, error }
}
