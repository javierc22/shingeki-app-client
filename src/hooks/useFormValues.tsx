import {useState, ChangeEvent} from 'react'

export const useFormValues = <T extends Object>( initialState:T ) => {
  const [values, setValues] = useState(initialState)

  const handleInputChange = ({target}: ChangeEvent<HTMLInputElement>) => {
    const {name, value} = target

    setValues({
      ...values, [name]: value
    })
  }

  return { values, handleInputChange }
}

