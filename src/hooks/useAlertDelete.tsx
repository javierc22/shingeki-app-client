import { useState } from 'react'
import { useHistory } from "react-router-dom";
import Swal from 'sweetalert2'
import withReactContent from 'sweetalert2-react-content'

import axiosClient from '../config/axiosClient'

const useAlertDelete = (title: string, url: string) => {
  const MySwal = withReactContent(Swal)
  const [loading, setLoading] = useState(false)
  const [error, setError] = useState(false)
  let history = useHistory()

  const handleDelete = () => {
    setLoading(true)
    setError(false)

    MySwal.fire({
      title: `${title}`,
      showDenyButton: true,
      showCancelButton: true,
      confirmButtonText: `Sí`,
      denyButtonText: `No`,
    }).then((result) => {
      if (result.isConfirmed) {
        axiosClient.delete(url)
          .then( res => {
            history.push(`/admin/titanes`)
            MySwal.fire('Eliminado!', '', 'success')
          }) 
          .catch(err => {
            setError(true)
            MySwal.fire('Algo salió mal. Intente nuevamente', '', 'error')
          })
      } else if (result.isDenied) {
        MySwal.fire('Los cambios no se guardaron', '', 'info')
      }
    })

    setLoading(false)
  }

  return { handleDelete, error, loading }
}

export default useAlertDelete
